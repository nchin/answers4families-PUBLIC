// package vars
var pkg = require("./package.json");

// gulp
var gulp = require("gulp");


// load all plugins in "devDependencies" into the variable $
var $ = require("gulp-load-plugins")({
            pattern: ["*"],
            scope: ["devDependencies"]
        });

var browserSync = require("browser-sync").create();

    var config = require('./sftp-config.json');

    /* list all files you wish to ftp in the glob variable */
    var sftpFiles = [
        'assets/dist/**/*'
        // '**/*',
        // '*',
        // '!bower_components/**/*',
        // '!node_modules/**/*' // if you wish to exclude directories, start the item with an !
    ];

function customPlumber(errTitle) {
  return $.plumber({
    errorHandler: $.notify.onError({
      // Customizing error title
      title: errTitle || "Error running Gulp",
      message: "Error: <%= error.message %>",
      sound: "Pop"
    })
  });
}

var banner = [
    "/**",
    " * @project        <%= pkg.name %>",
    " * @author         <%= pkg.author %>",
    " * @build          " + $.moment().format("llll") + " ET",
    " * @copyright      Copyright (c) " + $.moment().format("YYYY") + ", <%= pkg.copyright %>",
    " *",
    " */",
    ""
].join("\n");


/* ----------------- */
/* CSS GULP TASKS
/* ----------------- */

// scss - build the scss to the build folder, including the required paths, and writing out a sourcemap
gulp.task('scss', function() {
    $.fancyLog("-> Compiling scss: " + pkg.paths.build.css + pkg.vars.scssName);
    return gulp.src(pkg.paths.src.scss + "**/*.scss")
        .pipe(customPlumber('Error Running Sass'))
        .pipe($.sassGlob())
        .pipe($.sourcemaps.init({ loadMaps: true }))
        .pipe($.sass({
                includePaths: [
                    pkg.paths.src.scss + "**/*.scss"
                ]
            })
            .on('error', $.sass.logError))
        .pipe($.cached('sass_compile'))
        .pipe($.autoprefixer())
        .pipe($.sourcemaps.write('./'))
        .pipe($.size({ gzip: true, showFiles: true }))
        .pipe(gulp.dest(pkg.paths.build.css));
});

// css task - combine & minimize any vendor CSS into the public css folder
gulp.task("css", ["scss"], function() {
    $.fancyLog("-> Building css");
    return gulp.src(pkg.globs.distCss)
        .pipe(customPlumber('Error Running Sass'))
        .pipe($.newer({ dest: pkg.paths.dist.css + pkg.vars.siteCssName }))
        .pipe($.print())
        .pipe($.sourcemaps.init({ loadMaps: true }))
        .pipe($.concat(pkg.vars.siteCssName))
        .pipe($.cssnano({
            discardComments: {
                removeAll: true
            },
            discardDuplicates: true,
            discardEmpty: true,
            minifyFontValues: true,
            minifySelectors: true
        }))
        .pipe($.header(banner, { pkg: pkg }))
        .pipe($.sourcemaps.write("./"))
        .pipe($.size({ gzip: true, showFiles: true }))
        .pipe(gulp.dest(pkg.paths.dist.css))
        .pipe($.filter("**/*.css"))
        .pipe(browserSync.reload({stream:true}));
});

/* ----------------- */
/* JS GULP TASKS
/* ----------------- */
// babel js task - transpile our Javascript into the build directory
gulp.task("js-babel", function() {
    $.fancyLog("-> Transpiling Javascript via Babel...");
    return gulp.src(pkg.globs.babelJs)
        .pipe(customPlumber('Error Running Sass'))
        .pipe($.newer({ dest: pkg.paths.build.js }))
        // .pipe($.concat(pkg.vars.buildJsName))
        .pipe($.babel({
            presets: ['env']
        }))
        .pipe($.size({ gzip: true, showFiles: true }))
        .pipe(gulp.dest(pkg.paths.build.js));
});

// js task - minimize any distribution Javascript into the public js folder, and add our banner to it
gulp.task("js", ["js-babel"], function() {
    $.fancyLog("-> Building js");
    return gulp.src(pkg.globs.distJs)
        .pipe(customPlumber('Error Running Sass'))
        .pipe($.if(["*.js", "!*.min.js"],
            $.newer({ dest: pkg.paths.dist.js, ext: ".min.js" }),
            $.newer({ dest: pkg.paths.dist.js })
        ))
        .pipe($.concat(pkg.vars.jsName))
        .pipe($.if(["*.js", "!*.min.js"],
            $.uglify()
        ))
        .pipe($.if(["*.js", "!*.min.js"],
            $.rename({ suffix: ".min" })
        ))
        .pipe($.header(banner, { pkg: pkg }))
        .pipe($.size({ gzip: true, showFiles: true }))
        .pipe(gulp.dest(pkg.paths.dist.js))
        .pipe($.filter("**/*.js"))
        .pipe(browserSync.reload({stream:true}));
});

/* ----------------- */
/* MISC GULP TASKS
/* ----------------- */

// imagemin task
gulp.task("imagemin", function() {
    return gulp.src(pkg.paths.dist.img + "**/*.{png,jpg,jpeg,gif,svg}")
        .pipe($.imagemin({
            progressive: true,
            interlaced: true,
            optimizationLevel: 7,
            svgoPlugins: [{ removeViewBox: false }],
            verbose: true,
            use: []
        }))
        .pipe(gulp.dest(pkg.paths.dist.img));
});

// task to convert svg to data uri
gulp.task('sassvg', function(){
    return gulp.src(pkg.paths.src.svg + "/**/*.svg") 
        .pipe(sassvg({
          outputFolder: pkg.paths.dist.svg, // IMPORTANT: this folder needs to exist
      optimizeSvg: true // true (default) means about 25% reduction of generated file size, but 3x time for generating the _icons.scss file
        }));
});

// output plugin names in terminal
gulp.task('pluginOutput', function() {
    console.log($);
});

// Copy fonts
gulp.task('fonts', function() {
  return gulp.src(pkg.paths.src.fonts)
    .pipe(gulp.dest(pkg.paths.dist.fonts));
});

// Copy fonts
gulp.task('img', function() {
  return gulp.src(pkg.paths.src.img)
    .pipe(gulp.dest(pkg.paths.dist.img));
});

// task for deploying files on the server
gulp.task('deploy', function() {


    var conn = $.sftp({
        host: config.host,
        user: config.user,
        pass: config.password,
        remotePath: config.remote_path,
    });

    return gulp.src(sftpFiles, { base: '.', buffer: false })
        .pipe(conn)
        .pipe($.notify("Dev site updated!"))
        .pipe(browserSync.reload({stream:true}));
});


gulp.task('browsersync', function() {
      browserSync.init({
          port: 8080,
          //server: "./",
          proxy: 'http://www.answers4families.org:8080', // We need to use a proxy instead of the built-in server because Drupal has to do some server-side rendering for the theme to work
          // Open the site in Chrome & Firefox
          browser: ["google chrome", "firefox"]
      });
});



/* ----------------- */
/* Run TASKS
/* ----------------- */

// Default task
gulp.task("default", ["css", "js", "fonts", "img", "browsersync"], function() {


    gulp.watch([pkg.paths.src.scss + "**/*.scss"], ["css"]);
    gulp.watch([pkg.paths.src.js + "**/*.js"], ["js"]);
    gulp.watch([pkg.paths.templates ], () => {
        gulp.src(pkg.paths.templates)
            .pipe(customPlumber('Error Running Sass'))
            .pipe(browserSync.reload({stream:true}));
    });
    gulp.watch([pkg.paths.src.fonts],["fonts"]);
    gulp.watch([sftpFiles], ['deploy']);
});

// Production build
gulp.task("build", ["default", "imagemin"]);