"use strict";

document.addEventListener("DOMContentLoaded", function () {
    var accordion = document.querySelector(".accordion");

    if (accordion) {
        var triggers = document.querySelectorAll(".accordion-trigger");
        Array.prototype.forEach.call(triggers, function (trigger) {

            //hide all panels on page load
            trigger.classList.add('inactive');
            trigger.setAttribute("aria-expanded", "false");
            trigger.nextElementSibling.style.maxHeight = 0;

            function toggleVisible() {
                var thisButton = this;

                // toggle panel of button clicked
                if (thisButton.classList.contains('inactive')) {
                    //hide any expanded panels
                    var activeTrigger = document.querySelector('.accordion-trigger.active');
                    if (activeTrigger) {
                        activeTrigger.classList.remove('active');
                        activeTrigger.classList.add('inactive');
                        activeTrigger.setAttribute("aria-expanded", "false");
                        activeTrigger.nextElementSibling.style.maxHeight = 0;
                    }
                    // open panel for button click on
                    thisButton.classList.remove('inactive');
                    thisButton.classList.add('active');
                    thisButton.setAttribute("aria-expanded", "true");
                    var panel = thisButton.nextElementSibling;
                    panel.style.maxHeight = panel.scrollHeight + "px";
                } else if (thisButton.classList.contains('active')) {
                    thisButton.classList.remove('active');
                    thisButton.classList.add('inactive');
                    thisButton.setAttribute("aria-expanded", "false");
                    var panel = thisButton.nextElementSibling;
                    panel.style.maxHeight = 0;
                }
            }

            trigger.addEventListener("click", toggleVisible);
        });
    }
}, false);