This design system is set up according to BEMIT methodology

More info at http://www.creativebloq.com/web-design/manage-large-css-projects-itcss-101517528

https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/

01.Settings
------------
- These are preprocessor variables for later use.
- e.g., base font size, color palettes, config

02.Tools
------------
- These include all the mixins and functions.
- e.g., gradient mixins, font-sizing mixins

03.Generic
------------
- This includes all the third-party CSS used throughout the site.
- contains normalize.css, global box-sizing rule, CSS resets

04.Elements
------------
- These are element styles — no classes or IDs. Base typography styles go here, as well as anything needed to style raw elements.
- h1, etc

05.Objects
------------
- This layer is mostly layout-driven, and doesn’t include anything cosmetic. I included structural objects such as `.inner` and `.container` here, as well as some oft-repeated constructs such as `.post`s and `.hero`.
- separate structure from skin
- separate container from content

06.Components
------------
- This is mostly a cosmetic layer — this is where I included more specific styling instructions for objects. Without the components layer, the site would still have its structure, but would look like a gray-box wireframe.
- No heights on anything that contains text. Components should be flexible and their widths should be controlled by grids.

07.Trumps
------------
- These are styles that override other styles, and should be used very sparingly. Color overrides, forced floats, and text alignment classes can go here. This is the only place in your CSS that the `!important` tag is acceptable.

------------
Naming Convention
https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/
------------
o-
c-
u-
t-
s-
is-, has-
js-
qa-

Example:
.is-open {}

.has-dropdown {}

.js-modal {}

------------
Questions to ask
------------
how many pages does it appear on, and how many times could it appear on a given page? Thinking about the elements on a typical blog website, we could divide them as follows:

Appears on several pages, potentially several times per page:
Subheadings (h2, h3, etc)
Paragraphs
Most inline elements (a, em, strong)
Images, videos, audio players
‘Skip to’ links (see this post)

Appears on several pages, once on each page:
Site banner, including logo and strapline
Main navigation
Main article heading (h1)
Footer, including contact and social media links

Appears on one page, several times:
Article teasers on the landing page of the site

Appears on one page, once:
Contact form
Photo of the blogger's head
