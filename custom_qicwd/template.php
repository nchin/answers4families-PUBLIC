<?php
/**
 * @file
 * Theme functions
 */

// Include all files from the includes directory.
$includes_path = dirname(__FILE__) . '/includes/*.inc';
foreach (glob($includes_path) as $filename) {
    require_once dirname(__FILE__) . '/includes/' . basename($filename);
}

/**
 * Implements template_preprocess_page().
 */
function custom_qicwd_preprocess_page(&$variables)
{

  // Add copyright to theme.
    if ($copyright = theme_get_setting('copyright')) {
        $variables['copyright'] = check_markup($copyright['value'], $copyright['format']);
    }

  // Use a different template per content type.
  // if (isset($variables['node'])) {
  //   $variables['theme_hook_suggestions'][] = 'page__'. $variables['node']->type;
  // }
    if (isset($variables['node']->type)) {
      // If the content type's machine name is "my_machine_name" the file
      // name will be "page--my-machine-name.tpl.php".
      // If your content type is two or more words,
      //replace the underscore ( _ ) with a short dash ( - ) in the content type machine name.
        $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
    }

  // Add search_form to theme.
    $variables['search_form'] = '';
    if (module_exists('search') && user_access('search content')) {
        $search_box_form = drupal_get_form('search_form');
        $search_box_form['basic']['keys']['#title'] = 'Search';
        $search_box_form['basic']['keys']['#title_display'] = 'invisible';
        $search_box_form['basic']['keys']['#size'] = 20;
        $search_box_form['basic']['keys']['#attributes'] = array('placeholder' => 'Enter keyword ...');
        $search_box_form['basic']['keys']['#attributes']['class'][] = 'form-control';
        $search_box_form['basic']['submit']['#value'] = t('Search');
        $search_box_form['#attributes']['class'][] = 'c-header__search-form';
      // $search_box_form['#attributes']['class'][] = 'navbar-form';
      // $search_box_form['#attributes']['class'][] = 'navbar-right';
        $search_box = drupal_render($search_box_form);
        $variables['search_form'] = (user_access('search content')) ? $search_box : null;
    }
}

function custom_qicwd_preprocess_html(&$variables)
{

    // add Google fonts
    drupal_add_css('//fonts.googleapis.com/css?family=Libre+Franklin:400,700', array('group' => CSS_THEME));
}

// function custom_qicwd_preprocess_paragraph__banner(&$variables) {
//   $paragraph = $variables['paragraph'];
//   if (!$paragraph->field_image->isEmpty()) {
//     $image = $paragraph->field_image->entity->url();
//     $variables['attributes']['style'][] = 'background-image: url("' . $image . '");';
//     $variables['attributes']['style'][] = 'background-size: cover;';
//     $variables['attributes']['style'][] = 'background-position: center center;';
//   }
// }

function custom_qicwd_breadcrumb($variables)
{
    $breadcrumb = $variables['breadcrumb'];
    $crumbs = '';
    if (!empty($breadcrumb)) {
        $crumbs = '<nav class="breadcrumb u-mt-6@tablet" role="navigation" aria-label="You are here:"><ul class="u-pl-0 u-mb-0">';
        foreach ($breadcrumb as $value) {
            $crumbs .= '<li>' . $value . '</li>';
        }
        $crumbs .= '<li>' . drupal_get_title() . '</li></ul></nav>';
    }
    return $crumbs;
}

function custom_qicwd_panels_flexible($vars)
{

    $css_id = $vars['css_id'];
    $content = $vars['content'];
    $settings = $vars['settings'];
    $display = $vars['display'];
    $layout = $vars['layout'];
    $handler = $vars['renderer'];

    panels_flexible_convert_settings($settings, $layout);

    $renderer = panels_flexible_create_renderer(false, $css_id, $content, $settings, $display, $layout, $handler);

    $output = "<div class=\"panel-flexible " . $renderer->base['canvas'] . " clearfix\" $renderer->id_str>\n";
    $output .= "<div class=\"panel-flexible-inside " . $renderer->base['canvas'] . "-inside\">\n";

    $output .= panels_flexible_render_items($renderer, $settings['items']['canvas']['children'], $renderer->base['canvas']);

  // Wrap the whole thing up nice and snug
    $output .= "</div>\n</div>\n";

    return $output;
}
