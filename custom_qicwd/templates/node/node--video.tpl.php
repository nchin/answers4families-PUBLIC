<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php if ($teaser) { ?>
    <div class="o-layout__item">
      <div class="teaser__content u-mb-2">
        <?php if ($content['field_video_thumbnail']) : ?>
            <?php print render($content['field_video_thumbnail']); ?>
        <?php endif; ?>
      </div>
        <?php if (!$page && !empty($title)) : ?>
        <div<?php print $title_attributes; ?> class="teaser__title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></div>
        <?php endif; ?>
    </div>
<?php } else {  ?>
  <div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <?php print $user_picture; ?>

    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)) : ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted) : ?>
    <div class="submitted">
        <?php print t("Posted on ") . date("F j, Y", $node->created); ?>
    </div>
    <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

    <?php print render($content['links']); ?>

    <?php print render($content['comments']); ?>

</div>
<?php } ?>



