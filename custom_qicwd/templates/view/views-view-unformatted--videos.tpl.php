
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="o-layout o-layout--2">
    <?php foreach ($rows as $id => $row) : ?>
        <?php print $row; ?>
    <?php endforeach; ?>
</div>