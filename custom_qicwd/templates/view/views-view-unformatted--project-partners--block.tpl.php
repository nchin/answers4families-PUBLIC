<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="flex__container project-partners__container">
	<?php foreach ($rows as $id => $row): ?>
	  <div class="flex__item project-partner <?php if ($classes_array[$id]) { print $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </div>
	<?php endforeach; ?>
</div>