<?php

/**
 * @file
 * Default simple view template to display a group of summary lines.
 *
 * This wraps items in a span if set to inline, or a div if not.
 *
 * @ingroup views_templates
 */


//add ALL at end
foreach ($rows as $id => $row) {
  ?>

  <a href="<?php print $row->url;?>" class="btn btn-default u-mr-0">
    <?php print $row->link;?>
  </a>
    
  <?php if (!empty($options['count'])) {?>
      (<?php print $row->count;?>)
  <?php }?>
  <?php print !empty($options['inline']) ? '</span>' : '</div>';
}