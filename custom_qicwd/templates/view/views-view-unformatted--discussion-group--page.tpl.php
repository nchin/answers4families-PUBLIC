<?php

/**
 * @file
 * Custom view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="o-list-bordered--reverse">
	<?php foreach ($rows as $id => $row): ?>
	  <div class="o-list-bordered__item <?php if ($classes_array[$id]) { print $classes_array[$id];  } ?>">
	    <?php print $row; ?>
	  </div>
	<?php endforeach; ?>