<?php

/**
 * @file
 * View template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="u-display-flex u-flex-row u-flex-wrap u-jc-center u-bt u-b--white-transparent u-pt-5">
<?php foreach ($rows as $id => $row): ?>
  <div class="u-1/2 u-1/4@phablet u-display-inline-block <?php if ($classes_array[$id]) { print $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
