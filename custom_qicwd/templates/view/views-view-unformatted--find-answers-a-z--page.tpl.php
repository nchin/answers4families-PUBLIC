<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="u-bt u-b--gray-20 u-pv-4 u-ph-2">
	<?php if (!empty($title)): ?>
	  <h2 class="h5"><?php print $title; ?></h2>
	<?php endif; ?>
	
	<ul class="o-multicolumn u-mb-0">
	<?php foreach ($rows as $id => $row): ?>
	  <li<?php if ($classes_array[$id]) { print ' class="u-mb-2 ' . $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </li>
	<?php endforeach; ?>
	</ul>
</div>