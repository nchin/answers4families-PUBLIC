<?php if ($messages): ?>
  <div id="messages">
    <?php print $messages; ?>
  </div>
<?php endif; ?>

<header>
    <div class="c-header">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="c-header__logo u-1/4@tablet u-1/3@desktop u-pv-3">
            <?php if ($site_name || $logo): ?>
              <a href="<?php print $front_page; ?>" rel="home">
                <?php if ($logo): ?>
                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
                <?php endif; ?>
                <?php if ($site_name): ?>
                  <span class="sr-only"><?php print $site_name; ?></span>
                <?php endif; ?>
              </a>
            <?php endif; ?>
        </div><!-- /.navbar__logo -->

        <!-- Site-wide menu -->
        <nav class="c-header__primary" aria-label="site">
          <?php print theme('links', array('links' => menu_navigation_links('main-menu'), 'attributes' => array( 'class'=> array('list-inline', 'c-header__site-nav',)) ));?>
        </nav>

        <!-- Additional header features -->
        <div class="c-header__secondary u-1/4@desktop u-pv-3 u-pl-3">
          <!-- Site-wide user menu -->
          <nav class="c-header__user-menu u-pr-1" aria-label="user menu">

            <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#header__user-menu" aria-expanded="false" aria-controls="header__user-menu">
              <svg class="c-icon c-icon--small icon-keyhole" id="48b1c549-06d1-48ee-8274-a0376941b99b" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>Toggle user menu</title><path d="M18.86,6.86a6.86,6.86,0,0,0-13.71,0A6.69,6.69,0,0,0,8.4,12.69L6.86,23a1,1,0,0,0,.17.69,1.3,1.3,0,0,0,.69.34h8.57a1.3,1.3,0,0,0,.69-.34,1,1,0,0,0,.17-.69L15.6,12.69A6.69,6.69,0,0,0,18.86,6.86Zm-4.63,4.63a.94.94,0,0,0-.51.86l1.54,9.94H8.74l1.54-9.94a.94.94,0,0,0-.51-.86A4.88,4.88,0,0,1,6.86,6.86,5.05,5.05,0,0,1,12,1.71a5.05,5.05,0,0,1,5.14,5.14A4.88,4.88,0,0,1,14.23,11.49Z"/></svg>
            </button>

            <?php print theme('links', array('links' => menu_navigation_links('user-menu'), 'attributes' => array('id' => 'header__user-menu', 'class'=> array('collapse', 'list-inline', 'u-mb-3@desktop')) ));?>
          </nav>

          <!-- Search form -->
          <div class="c-header__search-container">
            <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#header__search-form" aria-expanded="false" aria-controls="header__search-form">
              <svg class="c-icon c-icon--small icon-search" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 19.66 19.71">
                <title>Toggle search form</title>
                <path d="M19.66,18.29l-5.36-5.36A8,8,0,0,0,8,0,8,8,0,0,0,2.34,13.66a8,8,0,0,0,10.53.68l5.37,5.37ZM3.76,12.24a6,6,0,1,1,8.48-8.48,6,6,0,1,1-8.48,8.48Z"/>
              </svg>
            </button>
            <div class="collapse c-header__search-form-container" id="header__search-form">
              <?php if ($search_form): ?>
                <?php print $search_form; ?>
              <?php endif; ?>
            </div>
          </div>

        </div>

    </div><!-- /.navbar -->
</header>
