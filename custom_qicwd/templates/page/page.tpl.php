<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>

<div class="o-sticky-wrapper">

    <?php require(drupal_get_path('theme', 'custom_qicwd') . '/templates/page/header.tpl.php'); ?>

    <main id="main">

      <div id="content" class="column o-wrapper">
        <div class="o-wrapper--text">

            <!-- <?php print theme('breadcrumb', array('breadcrumb'=>drupal_get_breadcrumb())); ?> -->

            <?php if ($page['breadcrumbs']) : ?>
            <?php print render($page['breadcrumbs']); ?>
            <?php endif; ?>

            <?php if ($title) : ?>
            <h1 class="page-title text-capitalize u-mb-4 <?php if (drupal_is_front_page()) :
?>sr-only<?php
                                                         endif; ?> <?php if (empty($page['breadcrumbs'])) :
?>u-mt-6<?php
                                                         endif; ?>"><?php print $title; ?></h1>
            <?php endif; ?>

            <?php if ($tabs) : ?>
            <div id="tabs">
                <?php print render($tabs); ?>
            </div>
            <?php endif; ?>

            <?php if ($action_links) : ?>
            <ul class="action-links">
                <?php print render($action_links); ?>
            </ul>
            <?php endif; ?>

            <?php if ($page['content_top']) : ?>
          <div id="content-top">
            <?php print render($page['content_top']); ?>
          </div>
            <?php endif; ?>

            <?php print render($page['content']); ?>

            <?php print $feed_icons; ?>

            <?php if ($page['content_bottom']) : ?>
            <div id="content-bottom">
                <?php print render($page['content_bottom']); ?>
            </div>
            <?php endif; ?>

            <?php if ($page['sidebar_first']) : ?>
            <div id="sidebar-first" class="column sidebar">
              <div class="section">
                <?php print render($page['sidebar_first']); ?>
              </div>
            </div> <!-- /.section, /#sidebar-first -->
            <?php endif; ?>

            <?php if ($page['sidebar_second']) : ?>
            <div id="sidebar-second" class="column sidebar">
              <div class="section">
                <?php print render($page['sidebar_second']); ?>
              </div>
            </div> <!-- /.section, /#sidebar-second -->
            <?php endif; ?>

        </div>
      </div><!-- /.section, /#content -->

    </main> <!-- /#main -->

</div>

<?php require(drupal_get_path('theme', 'custom_qicwd') . '/templates/page/footer.tpl.php'); ?>


