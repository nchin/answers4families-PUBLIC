<div class="<?php print $classes; ?> u-mb-6 u-text--small"<?php print $attributes; ?>>

  <span class="label-inline"><?php print $label; ?>:&nbsp;</span>
    <?php foreach ($items as $delta => $item) { ?>
      <span class="label label-default u-mr-1">
        <?php print render($item); ?>
      </span>
      <!-- <a class="label label-default" href="<?php print $item['#href']; ?>"><?php print $item['#title']; ?></a> -->
    <?php } ?>
</div>