<?php
/**
* @file
* Default theme implementation for a single paragraph item.
*
* Available variables:
* - $content: An array of content items. Use render($content) to print them
*   all, or print a subset such as render($content['field_example']). Use
*   hide($content['field_example']) to temporarily suppress the printing of a
*   given element.
* - $classes: String of classes that can be used to style contextually through
*   CSS. It can be manipulated through the variable $classes_array from
*   preprocess functions. By default the following classes are available, where
*   the parts enclosed by {} are replaced by the appropriate values:
*   - entity
*   - entity-paragraphs-item
*   - paragraphs-item-{bundle}
*
* Other variables:
* - $classes_array: Array of html class attribute values. It is flattened into
*   a string within the variable $classes.
*
* @see template_preprocess()
* @see template_preprocess_entity()
* @see template_process()
*/
?>
<?php
  $field_html_class = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_custom_html_class');
  $field_text_alignment = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_text_alignment');
  $field_link_file_upload = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_link_file_upload');
?>

<div class="<?php print $classes; ?> u-mb-6 <?php if (!empty($field_text_alignment[0]['value'])) :
?> <?php print $field_text_alignment[0]['value']; ?><?php
            endif; ?>" <?php print $attributes; ?>>

    <?php
    print render($content);
    ?>

</div>