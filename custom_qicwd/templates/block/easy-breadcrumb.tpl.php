<?php if ($segments_quantity > 0) : ?>
    <nav class="easy-breadcrumb breadcrumb u-mt-6@tablet" role="navigation" aria-label="You are here:">
        <ul class="u-pl-0 u-mb-0">
            <?php foreach ($breadcrumb as $i => $item) : ?>
                <li><?php print $item; ?></li>
            <?php endforeach; ?>
        </ul>
    </nav>
<?php endif; ?>